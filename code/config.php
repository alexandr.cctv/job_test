<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 8/13/18
 * Time: 10:47 PM
 */
$config = [
    'api_key' => '180413023455kb1d5c1223d347c115f78c6daaa9bd426252042511',
    'api_url' => 'https://api.recman.no/v2/get/',
    'fields' => [
        'job' => [
            'name',
            'startDate',
            'endDate',
            'salary',
            'invoice',
            'description',
            'salaryType',
            'separateInvoice',
            'holidayRate'
        ]
    ]
];