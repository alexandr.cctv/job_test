<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 8/13/18
 * Time: 11:09 PM
 */

namespace App;

/**
 * Class JobApi
 * @package App
 */
class JobApi
{
    private $apiKey;
    private $apiUrl;
    private $apiFields;

    /**
     * JobApi constructor.
     */
    public function __construct()
    {
        require_once './../config.php';
        $this->apiKey = $config['api_key'];
        $this->apiUrl = $config['api_url'];
        $this->apiFields = $config['fields'];
    }

    /**
     * Return array of jobs
     * @return mixed
     * @throws \Exception
     */
    public function getJobs():array
    {
        $response = file_get_contents($this->makeRequestUrl());

        $response = json_decode($response, true);

        $this->validateResponse($response);

        return $response['data'];
    }

    /**
     * Validate response
     * @param $response
     * @throws \Exception
     */
    private function validateResponse(&$response)
    {
        if ($response === null) {
            throw new \Exception('Json can`t be decoded');
        }

        if (!isset($response['success'])) {
            throw new \Exception('Undefined status of response');
        }

        if (!$response['success']) {
            throw new \Exception('Request return failed status');
        }
    }

    /**
     * make request url
     * @param string $scope
     * @return string
     */
    private function makeRequestUrl(string $scope = 'job'): string
    {

        return $this->apiUrl . '?key=' . $this->apiKey . '&scope=' . $scope . '&fields=' . implode(',', $this->apiFields[$scope]);
    }
}