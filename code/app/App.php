<?php
/**
 * Created by PhpStorm.
 * User: alex
 * Date: 8/13/18
 * Time: 10:51 PM
 */

namespace App;

/**
 * Class App
 * @package App
 */
class App
{
    protected $api;

    /**
     * App constructor.
     */
    public function __construct()
    {
        require_once 'JobApi.php';
        $this->api = new JobApi();
    }

    /**
     * @throws \Exception
     */
    public function response()
    {
        $jobs = $this->api->getJobs();

        require_once './../views/index.php';
    }
}