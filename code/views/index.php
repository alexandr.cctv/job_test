<html>
<head>
    <title>Job Test</title>
</head>
<body>
<?php echo '<p>Job list</p>'; ?>
<table>
    <tr>
        <th>JobId</th>
        <th>ProjectId</th>
        <th>Project name</th>
        <th>Start date</th>
        <th>End date</th>
        <th>Salary</th>
        <th>invoice</th>
        <th>description</th>
        <th>salaryType</th>
        <th>separateInvoice</th>
        <th>holidayRate</th>
    </tr>
    <?php foreach ($jobs as $job): ?>
        <tr>
            <td><?php echo $job['jobId'] ?></td>
            <td><?php echo $job['projectId'] ?></td>
            <td><?php echo $job['name'] ?></td>
            <td><?php echo $job['startDate'] ?></td>
            <td><?php echo $job['endDate'] ?></td>
            <td><?php echo $job['salary'] ?></td>
            <td><?php echo $job['invoice'] ?></td>
            <td><?php echo $job['description'] ?></td>
            <td><?php echo $job['salaryType'] ?></td>
            <td><?php echo $job['separateInvoice'] ?></td>
            <td><?php echo $job['holidayRate'] ?></td>
        </tr>
    <?php endforeach; ?>
</table>
</body>
</html>